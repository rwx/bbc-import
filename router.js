'use strict';

var router = require('express').Router();
var config = require('./controllers/config');

router.get('/', (req, res) => {
  res.redirect('/config');
});
router.get('/config', config.index);

module.exports = router;
