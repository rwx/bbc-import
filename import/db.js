'use strict';

var run = require('co');
var client = require('mongodb').MongoClient;
var dsn = 'mongodb://localhost/export';

module.exports.getUpdated = function(name) {
  return run(function*() {
    var db = yield client.connect(dsn);
    var collection = db.collection('updates');
    var item = yield collection.findOne({
      _id: name
    });

    yield db.close();

    return item ? item.updated : null;
  }).catch(console.log.bind(console));
};

module.exports.setUpdated = function(name, updated) {
  return run(function*() {
    var db = yield client.connect(dsn);
    var collection = db.collection('updates');
    var found = yield collection.findOne({
      _id: name
    });

    if (found) {
      yield collection.update({
        _id: name
      }, {
        $set: {
          updated: updated
        }
      });
    } else {
      yield collection.insert({
        _id: name,
        updated: updated
      });
    }
    yield db.close();
  }).catch(console.log.bind(console));
};

module.exports.updateCollection = function(name, items) {
  return run(function*() {
    var db = yield client.connect(dsn);
    var collection = db.collection(name);
    var _items = items.filter(item => !!item._id);

    for (var item of _items) {
      var found = yield collection.findOne({
        _id: item._id
      });

      if (found) {
        yield collection.update({
          _id: item._id
        }, {
          $set: item
        });
      } else {
        yield collection.insert(item);
      }
    }
    yield db.close();
    return _items;
  }).catch(console.log.bind(console));
};
