'use strict';

var parseString = require('xml2js').parseString;
var config = require('../config');
var soap = require('soap');


function fvs(method, params) {
  var header = {
    AuthHeader: {
      UserName: config.username,
      Password: config.password
    }
  };
  var result = new Promise(function(resolve, reject) {
    soap.createClient(config.url, function(err, client) {
      client.addSoapHeader(header, '', 'tns', 'http://www.fvs.lt/webservices');
      client[method](params, function(err, result) {
        //console.dir(client.lastRequest);
        if (err) {
          return reject(err);
        }
        var options = {
          explicitArray: false,
          trim: true
        };
        var cb = function(err, data) {
          if (err) {
            return reject(err);
          }
          var l1key = Object.keys(data)[0];
          var l2key = Object.keys(data[l1key])[0];
          resolve(data[l1key][l2key]);
        };

        if (result.sError) {
          return reject(result);
        }
        parseString(result.Xml, options, cb);
      });
    });
  });

  result.catch(console.log.bind(console));

  return result;
}

function addFvsParam(options, property, value) {
  switch (property) {

    case 'writeSchema':
      options.writeSchema = false;
      break;

    case 'tKoregavimoData':
      options.attributes = options.attributes || {};
      options.attributes['xmlns:xsd'] = 'http://www.w3.org/2001/XMLSchema';
      options.tKoregavimoData = {
        attributes: {
          'xsi:type': 'xsd:dateTime'
        },
        $value: value
      };
      break;

    default:
      break;
  }
}

module.exports.GetPrekiuRusisPozymiusXml = function() {
  var options = {};
  addFvsParam(options, 'writeSchema', false);

  var result = new Promise(resolve => {
    fvs('GetPrekiuRusisPozymiusXml', options).then(data => resolve(data));
  });

  result.catch(console.log.bind(console));

  return result;
};

module.exports.GetEinamiejiLikuciaiXml = function(date) {
  date = date || null;
  var params = {};
  addFvsParam(params, 'writeSchema', false);
  addFvsParam(params, 'tKoregavimoData', new Date(date).toISOString());

  var result = new Promise(resolve => {
    fvs('GetEinamiejiLikuciaiXml', params).then(data => resolve(data));
  });

  result.catch(console.log.bind(console));

  return result;
};

module.exports.GetSandeliusXml = function() {
  var params = {};
  addFvsParam(params, 'writeSchema', false);

  var result = new Promise(resolve => {
    fvs('GetSandeliusXml', params).then(data => resolve(data));
  });

  result.catch(console.log.bind(console));

  return result;
};

module.exports.GetPrekes = function(date) {
  date = date || null;
  var params = {};
  addFvsParam(params, 'writeSchema', false);
  addFvsParam(params, 'tKoregavimoData', new Date(date).toISOString());

  var result = new Promise(resolve => {
    fvs('GetPrekes', params).then(data => resolve(data));
  });

  result.catch(console.log.bind(console));

  return result;
};
