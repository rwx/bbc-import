'use strict';

var run = require('co');
var Fvs = require('./fvs');
var db = require('./db');

run(function*() {
  var items, updated, now = new Date();

  items = yield Fvs.GetSandeliusXml();
  items.map(item => item._id = item.kodas);
  yield db.updateCollection('sandeliai', items);

  updated = yield db.getUpdated('likuciai');
  items = yield Fvs.GetEinamiejiLikuciaiXml(updated);
  if (items && items.length) {
    items.map(item => item._id = (item.preke && item.sandelis) ? item.preke + '_' + item.sandelis : null);
    yield db.updateCollection('likuciai', items);
    yield db.setUpdated('likuciai', now);
  }

  updated = yield db.getUpdated('prekes');
  items = yield Fvs.GetPrekes(updated);
  if (items && items.length) {
    items.map(item => item._id = item.sKodas);
    yield db.updateCollection('prekes', items);
    yield db.setUpdated('prekes', now);
  }

  items = yield Fvs.GetPrekiuRusisPozymiusXml();
  items.map(item => item._id = (item.kodas && item.tipas) ? item.kodas + '_' + item.tipas : null);
  yield db.updateCollection('pozymiai', items);

}).catch(console.log.bind(console));
