'use strict';

var config = require('./config');
var express = require('express');
var app = express();

var path = require('path');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

var less = require('less-middleware');
app.use(less('/less', {
  dest: '/css',
  pathRoot: path.join(__dirname, 'public')
}));
app.use(express.static(path.join(__dirname, 'public')));

var db = require('monk')(config.dsn);
app.use(function(req, res, next) {
  req.db = db;
  next();
});

var logger = require('morgan');
app.use(logger('dev'));

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(require('./router'));

module.exports = app;
